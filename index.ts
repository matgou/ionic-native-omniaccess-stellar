/**
 * This is a template for new plugin wrappers
 *
 * TODO:
 * - Add/Change information below
 * - Document usage (importing, executing main functionality)
 * - Remove any imports that you are not using
 * - Remove all the comments included in this template, EXCEPT the @Plugin wrapper docs and any other docs you added
 * - Remove this note
 *
 */
import { Injectable } from '@angular/core';
import { Plugin, Cordova, CordovaProperty, CordovaInstance, InstanceProperty, IonicNativePlugin } from '@ionic-native/core';
import { Observable } from 'rxjs/Observable';

/**
 * @name Omniaccess Stellar
 * @description
 * This plugin does something
 *
 * @usage
 * ```typescript
 * import { OmniaccessStellar } from '@ionic-native/omniaccess-stellar';
 *
 *
 * constructor(private omniaccessStellar: OmniaccessStellar) { }
 *
 * ...
 *
 *
 * this.omniaccessStellar.functionName('Hello', 123)
 *   .then((res: any) => console.log(res))
 *   .catch((error: any) => console.error(error));
 *
 * ```
 */
@Plugin({
  pluginName: 'OmniaccessStellar',
  plugin: 'cordova-plugin-omniaccessstellar',
  pluginRef: 'OmniaccessStellar',
  repo: 'git+https://github.com/don/cordova-plugin-omniaccessstellar.git',
  install: '', // OPTIONAL install command, in case the plugin requires variables
  installVariables: [], // OPTIONAL the plugin requires variables
  platforms: ['Android', 'iOS']
})
@Injectable()
export class OmniaccessStellar extends IonicNativePlugin {

  /**
  * This function init OmniaccessStellar LBS
   * @param token {string} Some param to configure something
   * @return {Observable<any>} Returns a promise that resolves when something happens
   */
  @Cordova()
  init(token: string): Observable<any> {
    return; // We add return; here to avoid any IDE / Compiler errors
  }

}
