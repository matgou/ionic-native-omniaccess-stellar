/**
 * This is a template for new plugin wrappers
 *
 * TODO:
 * - Add/Change information below
 * - Document usage (importing, executing main functionality)
 * - Remove any imports that you are not using
 * - Remove all the comments included in this template, EXCEPT the @Plugin wrapper docs and any other docs you added
 * - Remove this note
 *
 */
import { IonicNativePlugin } from '@ionic-native/core';
import { Observable } from 'rxjs/Observable';
/**
 * @name Omniaccess Stellar
 * @description
 * This plugin does something
 *
 * @usage
 * ```typescript
 * import { OmniaccessStellar } from '@ionic-native/omniaccess-stellar';
 *
 *
 * constructor(private omniaccessStellar: OmniaccessStellar) { }
 *
 * ...
 *
 *
 * this.omniaccessStellar.functionName('Hello', 123)
 *   .then((res: any) => console.log(res))
 *   .catch((error: any) => console.error(error));
 *
 * ```
 */
export declare class OmniaccessStellar extends IonicNativePlugin {
    /**
     * This function init OmniaccessStellar LBS
     * @param token {string} Some param to configure something
     * @return {Observable<any>} Returns a promise that resolves when something happens
     */
    init(token: string): Observable<any>;
}
