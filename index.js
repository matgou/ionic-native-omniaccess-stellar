var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Cordova, IonicNativePlugin, Plugin } from '@ionic-native/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
/**
 * @name Omniaccess Stellar
 * @description
 * This plugin does something
 *
 * @usage
 * ```typescript
 * import { OmniaccessStellar } from '@ionic-native/omniaccess-stellar';
 *
 *
 * constructor(private omniaccessStellar: OmniaccessStellar) { }
 *
 * ...
 *
 *
 * this.omniaccessStellar.functionName('Hello', 123)
 *   .then((res: any) => console.log(res))
 *   .catch((error: any) => console.error(error));
 *
 * ```
 */
var OmniaccessStellar = (function (_super) {
    __extends(OmniaccessStellar, _super);
    function OmniaccessStellar() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * This function init OmniaccessStellar LBS
     * @param token {string} Some param to configure something
     * @return {Observable<any>} Returns a promise that resolves when something happens
     */
    /**
       * This function init OmniaccessStellar LBS
       * @param token {string} Some param to configure something
       * @return {Observable<any>} Returns a promise that resolves when something happens
       */
    OmniaccessStellar.prototype.init = /**
       * This function init OmniaccessStellar LBS
       * @param token {string} Some param to configure something
       * @return {Observable<any>} Returns a promise that resolves when something happens
       */
    function (token) {
        return; // We add return; here to avoid any IDE / Compiler errors
    };
    OmniaccessStellar.decorators = [
        { type: Injectable },
    ];
    __decorate([
        Cordova(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String]),
        __metadata("design:returntype", Observable)
    ], OmniaccessStellar.prototype, "init", null);
    /**
     * @name Omniaccess Stellar
     * @description
     * This plugin does something
     *
     * @usage
     * ```typescript
     * import { OmniaccessStellar } from '@ionic-native/omniaccess-stellar';
     *
     *
     * constructor(private omniaccessStellar: OmniaccessStellar) { }
     *
     * ...
     *
     *
     * this.omniaccessStellar.functionName('Hello', 123)
     *   .then((res: any) => console.log(res))
     *   .catch((error: any) => console.error(error));
     *
     * ```
     */
    OmniaccessStellar = __decorate([
        Plugin({
            pluginName: 'OmniaccessStellar',
            plugin: 'cordova-plugin-omniaccessstellar',
            pluginRef: 'OmniaccessStellar',
            repo: 'git+https://github.com/don/cordova-plugin-omniaccessstellar.git',
            install: '',
            // OPTIONAL install command, in case the plugin requires variables
            installVariables: [],
            // OPTIONAL the plugin requires variables
            platforms: ['Android', 'iOS']
        })
    ], OmniaccessStellar);
    return OmniaccessStellar;
}(IonicNativePlugin));
export { OmniaccessStellar };
//# sourceMappingURL=index.js.map
